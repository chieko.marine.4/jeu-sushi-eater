const bloclapin = document.querySelector<HTMLImageElement>(".bloc-lapin");
const boxMinuteur = document.querySelector<HTMLElement>("#box-minuteur");
const btnStart = document.querySelector<HTMLElement>("#btnStart");
const btnREsta = document.querySelector<HTMLElement>("#btnRestart");
const fin = document.querySelector<HTMLElement>("#box-fin");


import { lancerJeu } from "./entities";




/////////////Button start///////////////////////////

btnStart.addEventListener("click", () => {  

  lancerJeu();
  
  btnStart.style.display = "none"; 
  fin.style.display = "none";
  boxMinuteur.classList.add("pulse");
  
})

/////////////Button Restart///////////////////////////

btnREsta.addEventListener("click", () => {  

  location.reload()
  
})


/**  pour bouger le lapin avec keydownevent*/
  
bloclapin.style.bottom = "100" + "px";
bloclapin.style.left = "800" + "px";

document.addEventListener('keydown', (event) => {

   // lapin bouge
  const step = 10;

  if (event.key === 'ArrowLeft') {
  bloclapin.style.left = parseInt(bloclapin.style.left) - step + 'px';

  } else if (event.key === 'ArrowRight') {
    bloclapin.style.left = parseInt(bloclapin.style.left) + step + 'px';

  }

  });


  