const board = document.querySelector<HTMLElement>("#board");
const imgLapin = document.querySelector<HTMLImageElement>("#lapin");
const parolLapin = document.querySelector<HTMLImageElement>(".parolLapin");
const second = document.querySelector<HTMLElement>("#second");
const score = document.querySelector<HTMLElement>("#score");
const fin = document.querySelector<HTMLElement>("#box-fin");
const finscore = document.querySelector<HTMLElement>("#finscore");

export const tbImage = [
    "/img/thon.png",
    "/img/ika.png",
    "/img/inari.png",
    "/img/maki.png",
    "/img/sake.png",
    "/img/ikura.png"
]

let point = 0;
let temps = 15;


/** @function  lancerJeu */
/* function qui contient le timer, Generer le sushi, Condition de jeu et affihage de fin*/

export function lancerJeu(){

    second.textContent = temps.toString();
    temps--
    console.log(temps);

    creerSushi() ;
    setTimeout(() => { creerSushi() }, 500);

      // condition temps
    if(temps >= 0){

        setTimeout(() => {  
        lancerJeu()
        }, 1000)
    

    } else if(temps === -1){

        fin.style.display = "flex";
        finscore.innerHTML= point.toString();
        console.log(document.querySelectorAll(".sushi"));
}
    
}

/** @function detecter sushi lapin touche*/
/* function pour detecter de collision de 2 elements (Sushi et Lapin).
egalement il permet incrementer des points et condition pour que le lapin grandisse
avec les point*/

export function detecter(OBJ1: HTMLImageElement, OBJ2: HTMLImageElement) {

    document.addEventListener('keydown', () => {

    //detecter
    if (OBJ1.x < OBJ2.x + OBJ2.width && OBJ1.x + OBJ1.width > OBJ2.x &&
    OBJ1.y < OBJ2.y + OBJ2.height && OBJ1.height + OBJ1.y > OBJ2.y) {

    OBJ1.remove();

      //point score
    point++
    console.log(point);
    console.log(OBJ1+ " " + OBJ2);
    
    score.textContent = point.toString();
    parolLapin.textContent = "Miam...";

      //condition la taille de lapin
    if (point == 5) {
        imgLapin.style.width = "50" + "px";
        imgLapin.style.height = "100" + "px";
        imgLapin.src = "/img/usagi-mange2.png";

    } else if (point == 10) {
        imgLapin.style.width = "80" + "px";
        imgLapin.style.height = "130" + "px";
        imgLapin.src = "/img/usagi-mange.png";

    } else if (point == 15) {
        imgLapin.style.width = "110" + "px";
        imgLapin.style.height = "160" + "px";
        imgLapin.src = "/img/usagi-mange2.png";
        parolLapin.style.top =  "40" + "px";

    } else if (point == 20) {
        imgLapin.style.width = "150" + "px";
        imgLapin.style.height = "190" + "px";
        imgLapin.src = "/img/usagi-mange.png"
        parolLapin.style.top =  "70" + "px";

    }

    }
    });
}



/** @function creerSushi */
/* function qui permet creer des sushis avec createElement ;
avec le random ,les image de sushi et le positionX de sushi changent chaque fois qu'ils creent.
il contient d'autres function (fall, detecter) pour que les sushi tombent et detecter de colision avec lapin*/

export function creerSushi() {

    let randomS = Math.floor(Math.random() * 6);
    let randomP = Math.floor(Math.random() * 1000);
    //
    let randomSushi = document.createElement("img");
    randomSushi.src = tbImage[randomS];
    //randomSushi.classList.add("sushi");
    board.append(randomSushi);

    randomSushi.style.position = "absolute";
    randomSushi.style.width = "80" + "px";
    randomSushi.style.left = randomP + "px";
    
    fall(randomSushi, 1);
    
    detecter(randomSushi,imgLapin)
    
    setTimeout(() => {
    //randomSushi.style.display = "none";
    randomSushi.remove();
    }, 4000);

    return randomSushi
    
}

  /** @function fall */
  /* function pour que les images de sushi tombent dans un ecran */

const timer = (ms: number) => new Promise(res => setTimeout(res, ms));

export async function fall(objet: HTMLElement, num: number) {
  
    for (let step = 0; step < 1000; step++) {
    objet.style.top = step + 'px';
    await timer(num);
    }
}
////////////////////////////////


